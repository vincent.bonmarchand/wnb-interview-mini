// Copyright (c) 2021 Vincent BONMARCHAND
//
// This software is released under the MIT License.
// https://opensource.org/licenses/MIT
export class QueryStringParams {
  private paramsAndValues: string[];
  constructor() {
    this.paramsAndValues = [];
  }
  public push(key: string, value: any): void {
    value = encodeURIComponent(value.toString());
    this.paramsAndValues.push([key, value].join('='));
  }
  public toString = (): string => this.paramsAndValues.join('&');
}
