// Copyright (c) 2021 Vincent BONMARCHAND
//
// This software is released under the MIT License.
// https://opensource.org/licenses/MIT

import { QueryStringParams } from './query-string-params';

export class UrlBuilder {
  public url: string;
  public queryString: QueryStringParams;
  constructor(private baseUrl: string, private action: string, queryString?: QueryStringParams) {
    this.url = [baseUrl, action].join('/');
    this.queryString = queryString || new QueryStringParams();
  }
  public toString(): string {
    const queryString: string = this.queryString ? this.queryString.toString() : '';
    return queryString ? `${this.url}?${queryString}` : this.url;
  }
}
