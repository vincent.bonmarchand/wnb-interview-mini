import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VolunteerIdComponent } from './volunteer-id.component';

describe('VolunteerIdComponent', () => {
  let component: VolunteerIdComponent;
  let fixture: ComponentFixture<VolunteerIdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [VolunteerIdComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VolunteerIdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
