import { Component, Input, OnInit } from '@angular/core';

import { Application } from 'src/app/modules/volunteers/interfaces';

@Component({
  selector: 'app-volunteer-id',
  templateUrl: './volunteer-id.component.html',
  styleUrls: ['./volunteer-id.component.scss'],
})
export class VolunteerIdComponent implements OnInit {
  @Input() application: Application;
  constructor() {}

  ngOnInit() {}

  getDate(): Date {
    return new Date(this.application.createdDate);
  }
}
