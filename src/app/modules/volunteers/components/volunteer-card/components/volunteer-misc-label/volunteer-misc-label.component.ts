import { Component, Input, OnInit } from '@angular/core';

import { SubStatus } from 'src/app/modules/volunteers/enums';

@Component({
  selector: 'app-volunteer-misc-label',
  templateUrl: './volunteer-misc-label.component.html',
  styleUrls: ['./volunteer-misc-label.component.scss'],
})
export class VolunteerMiscLabelComponent implements OnInit {
  @Input() status: string;
  statusColorClasses = new Map<string, string>([
    [SubStatus.Application, 'wnb-red-light'],
    [SubStatus.Cancelled, 'wnb-gray'],
    [SubStatus.Discussion, 'wnb-red'],
    [SubStatus.InProgress, 'wnb-green-light'],
    [SubStatus.UserHasParticipated, 'wnb-green'],
    [SubStatus.WaitingManagerValidation, 'wnb-yellow-light'],
    [SubStatus.WaitingAssociationValidation, 'wnb-yellow'],
  ]);
  statusSentences = new Map<string, string>([
    [SubStatus.Application, 'Candidature'],
    [SubStatus.Cancelled, 'Annulé / refusé / non finalisé'],
    [SubStatus.Discussion, 'Echange en cours'],
    [SubStatus.InProgress, 'En cours de réalisation'],
    [SubStatus.UserHasParticipated, 'A participé'],
    [SubStatus.WaitingManagerValidation, 'En attente du manager'],
    [SubStatus.WaitingAssociationValidation, 'Date à valider'],
  ]);
  constructor() {}

  ngOnInit() {}
}
