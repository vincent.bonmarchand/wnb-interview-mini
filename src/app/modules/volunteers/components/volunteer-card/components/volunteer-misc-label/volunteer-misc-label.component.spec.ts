import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VolunteerMiscLabelComponent } from './volunteer-misc-label.component';

describe('VolunteerMiscLabelComponent', () => {
  let component: VolunteerMiscLabelComponent;
  let fixture: ComponentFixture<VolunteerMiscLabelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [VolunteerMiscLabelComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VolunteerMiscLabelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
