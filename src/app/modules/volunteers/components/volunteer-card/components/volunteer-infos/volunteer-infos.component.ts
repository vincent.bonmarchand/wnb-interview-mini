import { Component, Input, OnInit } from '@angular/core';
import { faMapMarkerAlt, faUser } from '@fortawesome/free-solid-svg-icons';

import { Application } from 'src/app/modules/volunteers/interfaces';
import { faHandPointRight } from '@fortawesome/free-regular-svg-icons';

@Component({
  selector: 'app-volunteer-infos',
  templateUrl: './volunteer-infos.component.html',
  styleUrls: ['./volunteer-infos.component.scss'],
})
export class VolunteerInfosComponent implements OnInit {
  @Input() application: Application;
  faMapMarkerAlt = faMapMarkerAlt;
  faUser = faUser;
  faHandPointRight = faHandPointRight;
  constructor() {}

  ngOnInit() {}
}
