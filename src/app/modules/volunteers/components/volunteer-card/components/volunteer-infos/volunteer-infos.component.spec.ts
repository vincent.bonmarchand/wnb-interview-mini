import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VolunteerInfosComponent } from './volunteer-infos.component';

describe('VolunteerInfosComponent', () => {
  let component: VolunteerInfosComponent;
  let fixture: ComponentFixture<VolunteerInfosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [VolunteerInfosComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VolunteerInfosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
