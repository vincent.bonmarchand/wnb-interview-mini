import { Component, Input, OnInit } from '@angular/core';

import { Application } from 'src/app/modules/volunteers/interfaces';
import { SubStatus } from 'src/app/modules/volunteers/enums';
import { intervalToDuration } from 'date-fns';

@Component({
  selector: 'app-volunteer-misc',
  templateUrl: './volunteer-misc.component.html',
  styleUrls: ['./volunteer-misc.component.scss'],
})
export class VolunteerMiscComponent implements OnInit {
  @Input() application: Application;
  descriptionContent = new Map<string, string>([
    [
      SubStatus.Application,
      'Ce volontaire souhaite participer à votre mission. Envoyez lui un message pour lui expliquer la mission et vos disponibilités pour un premier échange.',
    ],
    [
      SubStatus.WaitingManagerValidation,
      'La mission se déroulera sur le temps de travail du volontaire, l’accord du manager est requis. En attendant sa validation, n’hésitez pas à donner toutes les informations de la mission au volontaire',
    ],
    [
      SubStatus.WaitingManagerValidation,
      'La mission se déroulera sur le temps de travail du volontaire, l’accord du manager est requis. En attendant sa validation, n’hésitez pas à donner toutes les informations de la mission au volontaire',
    ],
  ]);
  constructor() {}

  ngOnInit() {}
  getDate(date: string) {
    return new Date(date);
  }

  getDuration(startDate: string, endDate: string) {
    const duration = intervalToDuration({ start: new Date(startDate), end: new Date(endDate) });
    return duration.hours;
  }
}
