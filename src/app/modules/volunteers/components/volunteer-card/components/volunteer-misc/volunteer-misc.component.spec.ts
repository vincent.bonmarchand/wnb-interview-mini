import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VolunteerMiscComponent } from './volunteer-misc.component';

describe('VolunteerMiscComponent', () => {
  let component: VolunteerMiscComponent;
  let fixture: ComponentFixture<VolunteerMiscComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [VolunteerMiscComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VolunteerMiscComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
