import { Component, Input, OnInit } from '@angular/core';

import { Application } from '../../interfaces';

@Component({
  selector: 'app-volunteer-card',
  templateUrl: './volunteer-card.component.html',
  styleUrls: ['./volunteer-card.component.scss'],
})
export class VolunteerCardComponent implements OnInit {
  @Input() application: Application;
  constructor() {}

  ngOnInit() {}
}
