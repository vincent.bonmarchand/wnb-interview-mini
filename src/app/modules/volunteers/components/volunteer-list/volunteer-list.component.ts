import { Component, OnDestroy, OnInit } from '@angular/core';

import { Application } from '../../interfaces';
import { Subscription } from 'rxjs';
import { VolunteersApiService } from '../../services/volunteers-api.service';

@Component({
  selector: 'app-volunteer-list',
  templateUrl: './volunteer-list.component.html',
  styleUrls: ['./volunteer-list.component.scss'],
})
export class VolunteerListComponent implements OnInit, OnDestroy {
  applications: Application[] = [];
  private sub: Subscription;
  constructor(private volunteersApi: VolunteersApiService) {}

  ngOnInit() {
    this.sub = this.volunteersApi.applications.subscribe((res) => {
      this.applications = res;
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
