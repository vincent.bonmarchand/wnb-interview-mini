import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VolunteersFiltersComponent } from './volunteers-filters.component';

describe('VolunteersFiltersComponent', () => {
  let component: VolunteersFiltersComponent;
  let fixture: ComponentFixture<VolunteersFiltersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [VolunteersFiltersComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VolunteersFiltersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
