import { Component, HostListener, Input, OnInit } from '@angular/core';

import { VolunteersApiService } from '../../../services/volunteers-api.service';

@Component({
  selector: 'app-filter-label',
  templateUrl: './filter-label.component.html',
  styleUrls: ['./filter-label.component.scss'],
})
export class FilterLabelComponent {
  @Input() color: string;
  @Input() label: string;
  @Input() number: number;
  @Input() status: string;
  @HostListener('click')
  onClick() {
    this.volunteerApi.filterByStatus(this.status);
  }
  constructor(private volunteerApi: VolunteersApiService) {}
}
