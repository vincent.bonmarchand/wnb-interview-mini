import { Component, OnInit } from '@angular/core';

import { SubStatus } from '../../enums';
import { VolunteerStatistics } from '../../interfaces';
import { VolunteersApiService } from '../../services/volunteers-api.service';

@Component({
  selector: 'app-volunteers-filters',
  templateUrl: './volunteers-filters.component.html',
  styleUrls: ['./volunteers-filters.component.scss'],
})
export class VolunteersFiltersComponent implements OnInit {
  statistics: VolunteerStatistics[] = [];
  totalApplications = 0;
  filtersLabels = [
    { color: 'wnb-red-light', label: 'Candidature', status: SubStatus.Application },
    { color: 'wnb-red', label: 'Echange', status: SubStatus.Discussion },
    { color: 'wnb-yellow', label: 'Date à valider', status: SubStatus.WaitingAssociationValidation },
    { color: 'wnb-yellow-light', label: 'En attente du manager', status: SubStatus.WaitingManagerValidation },
    { color: 'wnb-green-light', label: 'En cours', status: SubStatus.InProgress },
  ];
  constructor(private volunteersApi: VolunteersApiService) {}

  ngOnInit() {
    this.volunteersApi.getVolunteerStats().subscribe((res) => {
      this.statistics = res;
      this.getTotalApplications();
      this.getCurrentApplication();
    });
  }
  // i may have miss something but i felt like it was missing data into the stats.
  // i have the global applications number but i was missing the actual candidate status so i'll calculate it.
  getCurrentApplication() {
    let count = this.totalApplications;
    this.statistics.forEach((stat) => {
      if (stat.status !== SubStatus.Application) {
        count -= stat.count;
      }
    });
    this.statistics.push({ status: SubStatus.Application, count });
  }

  getTotalApplications() {
    this.statistics.forEach((stat) => {
      if (stat.status === SubStatus.Application) {
        this.totalApplications = stat.count;
      }
    });
  }
}
