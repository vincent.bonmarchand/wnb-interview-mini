import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VolunteerStatusComponent } from './volunteer-status.component';

describe('VolunteerStatusComponent', () => {
  let component: VolunteerStatusComponent;
  let fixture: ComponentFixture<VolunteerStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [VolunteerStatusComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VolunteerStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
