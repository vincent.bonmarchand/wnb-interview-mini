import { Component, OnInit } from '@angular/core';

import { SubStatus } from '../../enums';
import { VolunteerStatistics } from '../../interfaces';
import { VolunteersApiService } from '../../services/volunteers-api.service';

@Component({
  selector: 'app-volunteer-status',
  templateUrl: './volunteer-status.component.html',
  styleUrls: ['./volunteer-status.component.scss'],
})
export class VolunteerStatusComponent implements OnInit {
  statistics: VolunteerStatistics[] = [];
  subStatus = SubStatus;
  totalApplications = 0;
  constructor(private volunteersApi: VolunteersApiService) {}

  ngOnInit() {
    this.volunteersApi.getVolunteerStats().subscribe((res) => {
      this.statistics = res;
      this.getTotalApplications();
      this.getCurrentApplication();
    });
  }
  // I would have loved refactor all thoses parts since it's common to an another component.
  // But i was missing time as i lost my whole project yesterday ...
  getTotalApplications() {
    this.statistics.forEach((stat) => {
      if (stat.status === SubStatus.Application) {
        this.totalApplications = stat.count;
      }
    });
  }

  getCurrentApplication() {
    let count = this.totalApplications;
    this.statistics.forEach((stat) => {
      if (stat.status !== SubStatus.Application) {
        count -= stat.count;
      }
    });
    this.statistics.push({ status: 'APP_CALC', count });
  }

  getPct(n: number) {
    return (n / this.totalApplications) * 100;
  }
}
