export interface VolunteerStatistics {
  status: string;
  count: number;
}

export interface Company {
  name: string;
}

export interface VolunteerProfile {
  firstname: string;
  lastname: string;
  company: Company;
}

export interface CoordinatorProfile {
  firstname: string;
  lastname: string;
}

export interface Initiative {
  title: string;
  streetName: string;
  city: string;
  postalCode: string;
  country: string;
}

export interface Entry {
  dateBegin: Date;
  dateEnd: Date;
}

export interface Application {
  status: string;
  createdDate: Date;
  volunteerProfile: VolunteerProfile;
  coordinatorProfile: CoordinatorProfile;
  initiative: Initiative;
  id: string;
  entries: Entry[];
}
