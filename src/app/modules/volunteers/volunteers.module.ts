import { CommonModule } from '@angular/common';
import { DateFnsModule } from 'ngx-date-fns';
import { FilterLabelComponent } from './components/volunteers-filters/filter-label/filter-label.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { StatsExtractPipe } from './pipes/stats-extract.pipe';
import { TooltipModule } from 'ng2-tooltip-directive';
import { VolunteerCardComponent } from './components/volunteer-card/volunteer-card.component';
import { VolunteerIdComponent } from './components/volunteer-card/components/volunteer-id/volunteer-id.component';
import { VolunteerInfosComponent } from './components/volunteer-card/components/volunteer-infos/volunteer-infos.component';
import { VolunteerListComponent } from './components/volunteer-list/volunteer-list.component';
import { VolunteerMiscComponent } from './components/volunteer-card/components/volunteer-misc/volunteer-misc.component';
import { VolunteerMiscLabelComponent } from './components/volunteer-card/components/volunteer-misc-label/volunteer-misc-label.component';
import { VolunteerStatusComponent } from './components/volunteer-status/volunteer-status.component';
import { VolunteersComponent } from './volunteers.component';
import { VolunteersFiltersComponent } from './components/volunteers-filters/volunteers-filters.component';
import { VolunteersRoutingModule } from './volunteers-routing.module';
@NgModule({
  declarations: [
    VolunteersComponent,
    VolunteerListComponent,
    VolunteerStatusComponent,
    VolunteersFiltersComponent,
    VolunteerCardComponent,
    FilterLabelComponent,
    VolunteerIdComponent,
    VolunteerInfosComponent,
    VolunteerMiscComponent,
    VolunteerMiscLabelComponent,
    StatsExtractPipe,
  ],
  imports: [CommonModule, VolunteersRoutingModule, FontAwesomeModule, HttpClientModule, DateFnsModule, TooltipModule],
})
export class VolunteersModule {}
