import { TestBed } from '@angular/core/testing';

import { VolunteersApiService } from './volunteers-api.service';

describe('VolunteersApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: VolunteersApiService = TestBed.get(VolunteersApiService);
    expect(service).toBeTruthy();
  });
});
