import { Application, VolunteerStatistics } from '../interfaces';
import { BehaviorSubject, Observable } from 'rxjs';

import { ApiEndpointsService } from '@core/services/api-endpoints.service';
import { ApiHttpService } from '@core/services/api-http.service';
import { Injectable } from '@angular/core';
import { take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class VolunteersApiService {
  private applicationsSubject: BehaviorSubject<Application[]> = new BehaviorSubject<Application[]>([]);
  public applications: Observable<Application[]> = this.applicationsSubject.asObservable();

  constructor(private apiHttpService: ApiHttpService, private apiEndpointsService: ApiEndpointsService) {
    this.getApplications();
  }

  getVolunteerStats(): Observable<VolunteerStatistics[]> {
    return this.apiHttpService.get(this.apiEndpointsService.getVolunteerStatsEndpoint()).pipe(take(1)) as Observable<VolunteerStatistics[]>;
  }

  getApplications(): Observable<Application[]> {
    return this.apiHttpService
      .get(this.apiEndpointsService.getApplicationsEndpoint())
      .pipe(take(1))
      .subscribe((res: Application[]) => {
        this.applicationsSubject.next(res);
      });
  }

  filterByStatus(status: string) {
    return this.apiHttpService
      .get(this.apiEndpointsService.getApplicationsFilteredEndpoint(status))
      .pipe(take(1))
      .subscribe((res: Application[]) => {
        this.applicationsSubject.next(res);
      });
  }
}
