// Copyright (c) 2021 Vincent BONMARCHAND
//
// This software is released under the MIT License.
// https://opensource.org/licenses/MIT
export enum SubStatus {
  Application = 'APPLICATION',
  Discussion = 'DISCUSSION',
  WaitingAssociationValidation = 'WAITING_ASSOCIATION_VALIDATION',
  WaitingManagerValidation = 'WAITING_MANAGER_VALIDATION',
  InProgress = 'IN_PROGRESS',
  UserHasParticipated = 'USER_HAS_PARTICIPATED',
  Cancelled = 'CANCELLED',
}
