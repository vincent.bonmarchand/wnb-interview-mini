import { Pipe, PipeTransform } from '@angular/core';

import { VolunteerStatistics } from '../interfaces';

@Pipe({
  name: 'statsExtract',
})
export class StatsExtractPipe implements PipeTransform {
  transform(value: VolunteerStatistics[], status: string): number {
    let res = 0;
    value.forEach((stats) => {
      if (stats.status === status) {
        res = stats.count;
      }
    });
    return res;
  }
}
