import { DateFnsConfigurationService, DateFnsModule } from 'ngx-date-fns';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { fr } from 'date-fns/locale';

const frenchConfig = new DateFnsConfigurationService();
frenchConfig.setLocale(fr);
@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, AppRoutingModule, HttpClientModule, DateFnsModule.forRoot()],
  providers: [{ provide: DateFnsConfigurationService, useValue: frenchConfig }],
  bootstrap: [AppComponent],
})
export class AppModule {}
