// Copyright (c) 2021 Vincent BONMARCHAND
//
// This software is released under the MIT License.
// https://opensource.org/licenses/MIT

import { Injectable } from '@angular/core';

// I'm using this structure i found on internet, it tought it was pretty neat. On this project we're not using the real API_ENDPOINT
// but we're able to see how it would work in a real situation
@Injectable({
  providedIn: 'root',
})
export class Constants {
  public readonly API_ENDPOINT: string = 'https://6143a763c5b553001717d062.mockapi.io/api';
  public readonly API_MOCK_ENDPOINT: string = 'https://6143a763c5b553001717d062.mockapi.io/api';
}
