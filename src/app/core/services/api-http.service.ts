import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ApiHttpService {
  constructor(private http: HttpClient) {}

  // Refactor with generic Templating
  public get(url: string, options?: any): any {
    return this.http.get(url, options);
  }
}
