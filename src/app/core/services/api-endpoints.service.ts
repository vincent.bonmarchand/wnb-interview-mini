import { Constants } from '@config/constants';
import { Injectable } from '@angular/core';
import { QueryStringParams } from '@shared/classes/query-string-params';
import { UrlBuilder } from '@shared/classes/url-builder';

@Injectable({
  providedIn: 'root',
})
export class ApiEndpointsService {
  constructor(private constants: Constants) {}

  private createUrl(action: string, isMockAPI: boolean = false): string {
    const urlBuilder: UrlBuilder = new UrlBuilder(isMockAPI ? this.constants.API_MOCK_ENDPOINT : this.constants.API_ENDPOINT, action);
    return urlBuilder.toString();
  }

  private createUrlWithQueryParameters(action: string, queryStringHandler?: (queryStringParameters: QueryStringParams) => void): string {
    const urlBuilder: UrlBuilder = new UrlBuilder(this.constants.API_ENDPOINT, action);
    if (queryStringHandler) {
      queryStringHandler(urlBuilder.queryString);
    }
    return urlBuilder.toString();
  }

  private createUrlWithPathVariables(action: string, pathVariables: any[] = []): string {
    let encodedPathVariablesUrl = '';
    for (const pathVariable of pathVariables) {
      if (pathVariable !== null) {
        encodedPathVariablesUrl += `/${encodeURIComponent(pathVariable.toString())}`;
      }
    }
    const urlBuilder: UrlBuilder = new UrlBuilder(this.constants.API_ENDPOINT, `${action}${encodedPathVariablesUrl}`);
    return urlBuilder.toString();
  }

  getVolunteerStatsEndpoint(): string {
    return this.createUrl('stats');
  }

  getApplicationsEndpoint(): string {
    return this.createUrl('wishes');
  }

  getApplicationsFilteredEndpoint(statusFilter: string): string {
    return this.createUrlWithQueryParameters('wishes', (qs: QueryStringParams) => {
      qs.push('status', statusFilter);
    });
  }
}
