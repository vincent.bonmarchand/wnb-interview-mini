# Feedback

Thank you for taking the test, we are very happy that you dedicate some time for Wenabi 💛.

**_How many hours did you work on this mini project ?_**
About 27h BUT, i lost all my files yesterday because of a bad manipulation on my end.
So in fact as soon as i started from scratch yesterday, around ~15h

---

## You have something else to say ?

Before submitting your final work, tell us what you think about these different topics:

**Note**: It is totally **OK** for you to not have any feedback. We simply wanted to give a space for anyone who feels that this process can be improved and we are happy to hear it. Also, there are no right/wrong answers !

**_1 - How do you feel about the workload that was necessary to complete the mini project ? Would you say it was too much or too little ?_**
It was ok.
**_2 - How would you change this exam and/or examination process?_**
Maybe some more precisions about the volunteers tag ? I feel like it was missing one. But it was maybe attended to see how the candidate handle this situation.
