# Copyright (c) 2021 Vincent BONMARCHAND
#
# This software is released under the MIT License.
# https://opensource.org/licenses/MIT

# base image
FROM node:14.18.0-alpine as build-stage

# set working directory
WORKDIR /app

# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH

# install and cache app dependencies
COPY package.json /app/package.json
COPY package-lock.json /app/package-lock.json

RUN npm ci --prefer-offline --no-audit
RUN npm install -g @angular/cli@8.3.22

# add app
COPY . /app

# generate build
RUN npm run build:prod

# base image
FROM nginx:alpine

COPY --from=build-stage /app/nginx.conf /etc/nginx/nginx.conf
# copy artifact build from the 'build environment'
COPY --from=build-stage /app/dist/wnb-interview-mini-project /usr/share/nginx/html

# expose port 80
EXPOSE 80

# run nginx
CMD ["nginx", "-g", "daemon off;"]
